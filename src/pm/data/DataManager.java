package pm.data;

import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Shape;
import pm.file.FileManager;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;

    
    



    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        FileManager fileManager = (FileManager) app.getFileComponent();
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        Workspace reset = (Workspace)app.getWorkspaceComponent();
        reset.getCanvasBackground().getChildren().clear();
        reset.defaulinitLeftPanel();
        reset.reloadWorkspace();
        
        

    }
    public Workspace getWorkspace(){
        Workspace workspace  = (Workspace)app.getWorkspaceComponent();
        return workspace;
    }
}
