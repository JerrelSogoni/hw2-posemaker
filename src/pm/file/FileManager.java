package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.Window;
import javax.imageio.ImageIO;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonNumber;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;
import saf.ui.AppMessageDialogSingleton;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {
    //shapes attributes for loading and saving
    public static final String SHAPES = "Shape";
    public static final String RECTANGLE = "Rectangle";
    public static final String ELLIPSE = "Ellipse";
    public static final String COLORFILL = "Fill";
    public static final String COLORSTROKE = "Stroke";
    public static final String THICKNESS = "StrokeWidth";
    public static final String XAXIS = "X";
    public static final String YAXIS = "Y";
    public static final String HEIGHT = "Height";
    public static final String WIDTH = "Width";
    public static final String XCENTER = "CenterX";
    public static final String YCENTER = "CenterY";
    public static final String XRADIUS = "RadiusX";
    public static final String YRADIUS = "RadiusY";
    public static final String MOUSECLICKED = "MousePressed";
    public static final String MOUSEDRAGGED = "MouseDragged";
    public static final String MOUSERELEASED = "MouseReleased";
    public static final String MOUSEENTERED = "MouseEntered";
    public static final String MOUSEEXITED = "MouseExited";
    public static final String CANVAS = "Canvas";
    public static final String BACKGROUND = "Background";
    // Canvas attributes
    
    public static final String ONACTION = "OnAction";
    
    
           
            
            
    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     * 
     * @param data The data management component for this application.
     * 
     * @param filePath Path (including file name/extension) to where
     * to save the data to.
     * 
     * @throws IOException Thrown should there be an error writing 
     * out data to the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
       filePath = filePath + "json";
       StringWriter sw = new StringWriter();
       DataManager dataManager = (DataManager)data;
       JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
       Workspace ourWorkspace = dataManager.getWorkspace();
       ourWorkspace.deselectShape();
       fillCanvasInfo(ourWorkspace, arrayBuilder);
       JsonArray nodesArray = arrayBuilder.build();
       Color background = dataManager.getWorkspace().getBackgroundFill().getValue();
       JsonObject dataManagerJSO = Json.createObjectBuilder()
		.add(CANVAS, nodesArray)
                //Canvas Info
		.add(BACKGROUND,  background.toString())
		.build();
       	// AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
	Map<String, Object> properties = new HashMap<>(1);
	properties.put(JsonGenerator.PRETTY_PRINTING, true);
	JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
	JsonWriter jsonWriter = writerFactory.createWriter(sw);
	jsonWriter.writeObject(dataManagerJSO);
	jsonWriter.close();

	// INIT THE WRITER
	OutputStream os = new FileOutputStream(filePath);
	JsonWriter jsonFileWriter = Json.createWriter(os);
	jsonFileWriter.writeObject(dataManagerJSO);
	String prettyPrinted = sw.toString();
	PrintWriter pw = new PrintWriter(filePath);
	pw.write(prettyPrinted);
	pw.close();
    }
      
    /**
     * This method loads data from a JSON formatted file into the data 
     * management component and then forces the updating of the workspace
     * such that the user may edit the data.
     * 
     * @param data Data management component where we'll load the file into.
     * 
     * @param filePath Path (including file name/extension) to where
     * to load the data from.
     * 
     * @throws IOException Thrown should there be an error reading
     * in data from the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
	DataManager dataManager = (DataManager)data;
	dataManager.reset();
	// LOAD THE JSON FILE WITH ALL THE DATA
	JsonObject json = loadJSONFile(filePath);
	JsonArray jsonCanvasArray = json.getJsonArray(CANVAS);
        for(int startingArray = 0; startingArray < jsonCanvasArray.size(); startingArray++){
            JsonObject ourShape = jsonCanvasArray.getJsonObject(startingArray);
            loadShapeInfo(ourShape, dataManager);
            
        }
        Paint backgroundColor = Paint.valueOf(json.getString(BACKGROUND));
        loadCanvasInfo(backgroundColor, dataManager);
        dataManager.getWorkspace().reloadWorkspace();
        
        
	
        

    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
	InputStream is = new FileInputStream(jsonFilePath);
	JsonReader jsonReader = Json.createReader(is);
	JsonObject json = jsonReader.readObject();
	jsonReader.close();
	is.close();
	return json;
    }
    
    /**
     * This method exports the contents of the data manager to a 
     * Web page including the html page, needed directories, and
     * the CSS file.
     * 
     * @param data The data management component.
     * 
     * @param filePath Path (including file name/extension) to where
     * to export the page to.
     * 
     * @throws IOException Thrown should there be an error writing
     * out data to the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {

    }
    
    /**
     * This method is provided to satisfy the compiler, but it
     * is not used by this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
	// NOTE THAT THE Web Page Maker APPLICATION MAKES
	// NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
	// EXPORTED WEB PAGES
    }
    int maxNodeCounter;
    private void fillCanvasInfo( Workspace workspace, JsonArrayBuilder arrayBuilder){
	
	// FIRST Shapes
	ObservableList<Node> shapes = workspace.getCanvasBackground().getChildren();
        for(int startOfLoop = 0; startOfLoop < shapes.size(); startOfLoop++){
            
            JsonObject ShapeObject = makeShapeJsonObject((Shape)shapes.get(startOfLoop));
            arrayBuilder.add(ShapeObject);
        }
	


        
        
    }
    private JsonObject makeShapeJsonObject( Shape shape){
        if(shape instanceof Rectangle){
            	JsonObject json = Json.createObjectBuilder()
		.add(SHAPES, RECTANGLE)
		.add(XAXIS, ((Rectangle)shape).getX())
                .add(YAXIS, ((Rectangle)shape).getY())
                .add(WIDTH, ((Rectangle)shape).getWidth())
                .add(HEIGHT, ((Rectangle)shape).getHeight())
                .add(COLORFILL, shape.getFill().toString())
                .add(COLORSTROKE, shape.getStroke().toString())
                .add(THICKNESS, shape.getStrokeWidth())
		.build();
                return json;
            
        }
        else{
            JsonObject json = Json.createObjectBuilder()
            .add(SHAPES, ELLIPSE)
            .add(XCENTER, ((Ellipse)shape).getCenterX())
            .add(YCENTER, ((Ellipse)shape).getCenterY())
            .add(XRADIUS, ((Ellipse)shape).getRadiusX())
            .add(YRADIUS, ((Ellipse)shape).getRadiusY())
            .add(COLORFILL, shape.getFill().toString())
            .add(COLORSTROKE, shape.getStroke().toString())
            .add(THICKNESS, shape.getStrokeWidth())
            .build();
            return json;
                
        }
        
        
        
        
    }
    private void loadCanvasInfo(Paint p, DataManager data){
        data.getWorkspace().getCanvasBackground().setBackground(new Background(new BackgroundFill( p, CornerRadii.EMPTY, Insets.EMPTY)));
        data.getWorkspace().getBackgroundFill().setValue((Color)p);
        
        
    }
    private void loadShapeInfo(JsonObject shape, DataManager data){
        String specificShape = shape.getString(SHAPES);
        double thickness = ((JsonNumber)shape.get(THICKNESS)).doubleValue();
        Paint outlineFill = Paint.valueOf(shape.getString(COLORSTROKE));
        Paint colorFill = Paint.valueOf(shape.getString(COLORFILL));
        Shape ourShape;
        if(specificShape.equals(RECTANGLE)){
            double x = ((JsonNumber)shape.get(XAXIS)).doubleValue();
            double y = ((JsonNumber)shape.get(YAXIS)).doubleValue();
            double width = ((JsonNumber)shape.get(WIDTH)).doubleValue();
            double height = ((JsonNumber)shape.get(HEIGHT)).doubleValue();
            ourShape = data.getWorkspace().makeShape(new Rectangle(), x, y, outlineFill, colorFill, thickness);
            ((Rectangle)ourShape).setWidth(width);
            ((Rectangle)ourShape).setHeight(height);
            
            
        }
        else{
            double centerX = ((JsonNumber)shape.get(XCENTER)).doubleValue();
            double centerY = ((JsonNumber)shape.get(YCENTER)).doubleValue();
            double radiusX = ((JsonNumber)shape.get(XRADIUS)).doubleValue();
            double radiusY = ((JsonNumber)shape.get(YRADIUS)).doubleValue();
            ourShape = data.getWorkspace().makeShape(new Ellipse(), centerX, centerY, outlineFill, colorFill, thickness);
            ((Ellipse)ourShape).setRadiusX(radiusX);
            ((Ellipse)ourShape).setRadiusY(radiusY);
            
        }
        
         data.getWorkspace().getCanvasBackground().getChildren().add(ourShape);
        
    }
   
}
