package pm.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import pm.controller.PageEditController;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import static saf.settings.AppStartupConstants.FILE_PROTOCOL;
import static saf.settings.AppStartupConstants.PATH_IMAGES;
import saf.ui.AppMessageDialogSingleton;
import pm.file.FileManager;


/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {
    public static final String LEFT_PANE_PROPERTIES = "left_pane_properties";
    public static final String LEFT_PANE_INNER_BOXES = "left_pane_inner_boxes";
    public static final String SQUARE_BUTTON_PROPERTIES = "square_buttons";
    public static final String RECTANGE_BUTTON_PROPERTIES = "rectangle_buttons";
    public static final String LABEL_PROPERTIES = "labelsForInnerBoxes";
    public static final String SELECTOR_PROPERTIES = "colorPicker_Properties";
    public static final String SNAP_SHOT_PROPERTIES = "snapShotButton";
    public static final String CANVAS_PROPERTIES = "canvas";
    public final int SLIDERMAX = 10;
    public static final double HIGHLIGHT_SIZE = 5;
    public static final Paint HIGHLIGHT_COLOR = Paint.valueOf("#FFFF00");
    // HERE'S THE APP
    AppTemplate app;
    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;
    // Use borderpane left for our left area , center for our canvas
    BorderPane ourWorkingArea; 
    // our workspace
    Pane canvasBackgroundPane;

    // stackpane to set panes on top of panes
    VBox leftPane;
    PageEditController PageEditController;
    // slider array for our sliders
    Slider thicknessSlider;
    //
    HBox ButtonArea;
    HBox ButtonLayering;
    VBox BackgroundColorBox;
    VBox OutlinePickerBox;
    VBox FillPickerBox;
    VBox ThicknessSliderBox;
    HBox CameraArea;
    private ToggleButton selector;
    private ToggleButton deleter;
    private ToggleButton square;
    private ToggleButton ellipse;
    private ToggleButton incLayer;
    private ToggleButton decLayer;
    private ToggleButton snapShot;
    private ColorPicker backgroundFill;
    private ColorPicker outlineFill;
    private ColorPicker fillColor;
    private Paint tempBeforeHighlight;
    private double tempBeforeHighlightThickness;
    private boolean shapePressedAlready;
    private Shape selectedShape;


    

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {

	// KEEP THIS FOR LATER
	app = initApp;
	// KEEP THE GUI FOR LATER
        gui = app.getGUI();
        // create page edit controller for editing for future use
        PageEditController = new PageEditController((PoseMaker) app);
        // work area will consist of leftpane for controls and center for canvas. using borderPane
        ourWorkingArea = new BorderPane();
        // file manager
        FileManager fileHandler = (FileManager)app.getFileComponent();
        // our shapes
        // left pane for inputs
        leftPane = new VBox();
        //Make our buttons such as selector, circle creator ...
        ButtonArea = new HBox();
        selector = new ToggleButton();
        selector.setId("SelectionTool");
        //Attach Image on eachButton
        imageAttacher(selector, selector.getId());
        selector.setDisable(true);
        selector.setOnAction(e ->{
            resetButtonIndicator();
            selector.setSelected(true);
            app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
        });
        deleter = new ToggleButton();
        deleter.setId("Remove");
        imageAttacher(deleter, deleter.getId());
        deleter.setDisable(true);
        deleter.setOnAction(e ->{
            resetButtonIndicator();
            deleter.setSelected(true);
            if(deleter.isSelected()){
                    if(shapePressedAlready){
                        canvasBackgroundPane.getChildren().remove(selectedShape);
                        deselectShape();
                        reloadWorkspace();
                }
            }
        });
        square = new ToggleButton();
        square.setId("Rect");
        imageAttacher(square, square.getId());
        square.setOnAction(e ->{
            resetButtonIndicator();
            if(shapePressedAlready){
                deselectShape();
            }
            square.setSelected(true);
            app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);
        });
        ellipse = new ToggleButton();
        ellipse.setId("Ellipse");
        imageAttacher(ellipse, ellipse.getId());
        ellipse.setOnAction(e -> {
            resetButtonIndicator();
            if(shapePressedAlready){
                deselectShape();
            }
            ellipse.setSelected(true);
            app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);
        });
        ButtonArea.getChildren().addAll(selector, deleter, square, ellipse);
        // Setup layering buttons
        ButtonLayering = new HBox();
        incLayer = new ToggleButton();
        incLayer.setId("Increment");
        imageAttacher(incLayer, incLayer.getId());
        incLayer.setOnAction(e ->{
            decLayer.setSelected(false);
            incLayer.setSelected(true);
            if(shapePressedAlready){
                ((Node)selectedShape).toBack();
                updateGUIButtonsSelectedShapes(selectedShape);
               
            }
        });
        decLayer = new ToggleButton();
        decLayer.setId("Decrement");
        imageAttacher(decLayer, decLayer.getId());
        decLayer.setOnAction(e ->{
            incLayer.setSelected(false);
            decLayer.setSelected(true);
            if(shapePressedAlready){
                ((Node)selectedShape).toFront();
                updateGUIButtonsSelectedShapes(selectedShape);
                
                
               
            }
        });
        incLayer.setDisable(true);
        decLayer.setDisable(true);
        ButtonLayering.getChildren().addAll(incLayer, decLayer);
        // Background Color box
        BackgroundColorBox = new VBox();
        Label backgroundColorText = new Label("Background Color");
        backgroundColorText.getStyleClass().add(LABEL_PROPERTIES);
        backgroundFill = new ColorPicker();
        backgroundFill.setOnAction(e ->{
            Paint backgroundColor =  backgroundFill.getValue();
            PageEditController.backgroundColorHandler(backgroundColor);

        });
        BackgroundColorBox.getChildren().addAll(backgroundColorText, backgroundFill);
        //Fill color Box and properties
        FillPickerBox = new VBox();
        Label FillPickerText = new Label("Fill Color");
        FillPickerText.getStyleClass().add(LABEL_PROPERTIES);
        fillColor = new ColorPicker();
        fillColor.setOnAction(e ->{
            if(shapePressedAlready){
                
                selectedShape.setFill(fillColor.getValue());
                
            }
        });
        FillPickerBox.getChildren().addAll(FillPickerText, fillColor);
        // outline Color Box and properties
        OutlinePickerBox = new VBox();
        Label OutlinePickerText = new Label("Outline Color");
        OutlinePickerText.getStyleClass().add(LABEL_PROPERTIES);
        outlineFill = new ColorPicker();
        outlineFill.setOnAction(e ->{
            if(shapePressedAlready){
                tempBeforeHighlight = outlineFill.getValue();
            }
        });
        OutlinePickerBox.getChildren().addAll(OutlinePickerText, outlineFill);
        // outline thickness
        ThicknessSliderBox = new VBox();
        Label ThicknessText = new Label("Outline Thickness");
        ThicknessText.getStyleClass().add(LABEL_PROPERTIES);
        thicknessSlider = new Slider();
        thicknessSlider.setOnMouseDragged(e ->{
                if(shapePressedAlready){
                    tempBeforeHighlightThickness = thicknessSlider.getValue();
                    selectedShape.setStrokeWidth(tempBeforeHighlightThickness);
                    selectedShape.setStroke(tempBeforeHighlight);
                }
                
            
        });
        thicknessSlider.setOnMouseReleased(e ->{
            if(shapePressedAlready){
                highlightShape(selectedShape);
            }
        });
        thicknessSlider.setMax(SLIDERMAX);
        ThicknessSliderBox.getChildren().addAll(ThicknessText, thicknessSlider);
        //Snapshot Button
        CameraArea = new HBox();
        snapShot = new ToggleButton();
        snapShot.setId("Snapshot");
        imageAttacher(snapShot, snapShot.getId());
        snapShot.setOnAction(e ->{
            resetButtonIndicator();
            snapShot.setSelected(true);
            WritableImage ourCanvas = canvasBackgroundPane.snapshot(new SnapshotParameters(), null);
            PageEditController.SnapShot(ourCanvas);
            resetButtonIndicator();
        
        });
        CameraArea.getChildren().add(snapShot);
      
        leftPane.getChildren().addAll(ButtonArea, ButtonLayering,
                                      BackgroundColorBox, FillPickerBox, OutlinePickerBox,
                                       ThicknessSliderBox, CameraArea );
        // setup our drawing area with background of the default color picker
        canvasBackgroundPane = new Pane();
        Paint backgroundColor =  backgroundFill.getValue();
        canvasBackgroundPane.setId("Background");
        canvasBackgroundPane.setBackground(new Background( new BackgroundFill( backgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
        canvasBackgroundPane.setOnMousePressed(e ->{
                double xaxis = e.getX();
                double yaxis = e.getY();
                double thickness = thicknessSlider.getValue();
                Paint outlineColor = (Paint)outlineFill.getValue();
                Paint colorFill = (Paint)fillColor.getValue();
                if(square.isSelected()){
                    Rectangle initRectangle = (Rectangle)makeShape(new Rectangle(), xaxis, yaxis, outlineColor, colorFill, thickness);
                    canvasBackgroundPane.getChildren().add(initRectangle);
                }
                else if(ellipse.isSelected()){
                    Ellipse initEllipse= (Ellipse)makeShape(new Ellipse(), xaxis, yaxis, outlineColor, colorFill, thickness);
                    canvasBackgroundPane.getChildren().add(initEllipse);
                    
                }
                else if(selector.isSelected() && !(e.getPickResult().getIntersectedNode().getClass().getName().contains("Rectangle") || e.getPickResult().getIntersectedNode().getClass().getName().contains("Ellipse"))){
                    if(shapePressedAlready){
                        deselectShape();
                    }

                }

   
        });
        canvasBackgroundPane.setOnMouseDragged(e ->{
            int ourShapeIndex = canvasBackgroundPane.getChildren().size() - 1;
            if(square.isSelected()){
                
                Rectangle modifiedRectangle = (Rectangle)canvasBackgroundPane.getChildren().get(ourShapeIndex);
                double xaxis = e.getX();
                double yaxis = e.getY();
                double height = e.getY() - modifiedRectangle.getY();
                double width = e.getX() - modifiedRectangle.getX();
                if(!(width < 0 || height < 0)){
                   modifiedRectangle.setHeight(height);
                   modifiedRectangle.setWidth(width);
               }
           }
            else if(ellipse.isSelected()){
                Ellipse modifiedEllipse = (Ellipse)canvasBackgroundPane.getChildren().get(ourShapeIndex);
                double height = e.getY() - modifiedEllipse.getCenterY();
                double width = e.getX() - modifiedEllipse.getCenterX();
                if(!(width <= 0 || height <= 0)){
                   modifiedEllipse.setRadiusX(width);
                   modifiedEllipse.setRadiusY(height);
               }
            }
        });
        canvasBackgroundPane.setOnMouseReleased(e ->{
            int ourShapeIndex = canvasBackgroundPane.getChildren().size() - 1;
            if(square.isSelected()){
                Rectangle modifiedRectangle = (Rectangle)canvasBackgroundPane.getChildren().get(ourShapeIndex);
                double height = e.getY() - modifiedRectangle.getY();
                double width = e.getX() - modifiedRectangle.getX();
                if(!(width <= 0 || height <= 0)){
                    modifiedRectangle.setHeight(height);
                    modifiedRectangle.setWidth(width);
                    selector.setDisable(false);
                    
                }
                else{
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Alert", "Cannot Make Rectangle Make sure More than Height and Width 0 and 0");
                    canvasBackgroundPane.getChildren().remove(canvasBackgroundPane.getChildren().size() - 1,canvasBackgroundPane.getChildren().size());
                    
                }
                app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);


            }
            if(ellipse.isSelected()){
                Ellipse modifiedEllipse = (Ellipse)canvasBackgroundPane.getChildren().get(ourShapeIndex);
                double xaxis = e.getX();
                double yaxis = e.getY();
                double height = e.getY() - modifiedEllipse.getCenterY();
                double width = e.getX() - modifiedEllipse.getCenterX();
                double rangeX = xaxis - modifiedEllipse.getStrokeWidth() - (modifiedEllipse.getRadiusX() * 2);
                double rangeY = yaxis - modifiedEllipse.getStrokeWidth() - (modifiedEllipse.getRadiusY() * 2);
                if(!(width <= 0 || height <= 0) && !(rangeX < 0 || rangeY < 0)){
                   modifiedEllipse.setRadiusX(width);
                   modifiedEllipse.setRadiusY(height);
                   selector.setDisable(false);
                }
                else{
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Alert", "Cannot Make Ellipse outside its Canvas\nPlease Make Sure You Draw On Canvas Or More than Height and Width 0 and 0");
                    canvasBackgroundPane.getChildren().remove(canvasBackgroundPane.getChildren().size() - 1,canvasBackgroundPane.getChildren().size());
                }
                app.getGUI().getPrimaryScene().setCursor(Cursor.CROSSHAIR);
                System.out.println(app.getGUI().getPrimaryScene().getCursor());
                    
            }

        });
        

        ourWorkingArea.setLeft(leftPane);

        ourWorkingArea.setCenter(canvasBackgroundPane);
        // constuct our workspace with our left and center outline
        setWorkspace(ourWorkingArea);


    }
    
    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
	// NOTE THAT EACH CLASS SHOULD CORRESPOND TO
	// A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
	// CSS FILE
        
        //Leftpane properties
        leftPane.getStyleClass().add(LEFT_PANE_PROPERTIES);
        //canvas background
        canvasBackgroundPane.getStyleClass().add(CANVAS_PROPERTIES);
        // 4 top button properties
        ButtonArea.getStyleClass().add(LEFT_PANE_INNER_BOXES);
        getSelector().getStyleClass().add(SQUARE_BUTTON_PROPERTIES);
        getDeleter().getStyleClass().add(SQUARE_BUTTON_PROPERTIES);
        getSquare().getStyleClass().add(SQUARE_BUTTON_PROPERTIES);
        getEllipse().getStyleClass().add(SQUARE_BUTTON_PROPERTIES);
        //Layering properties
        ButtonLayering.getStyleClass().add(LEFT_PANE_INNER_BOXES);
        getIncLayer().getStyleClass().add(RECTANGE_BUTTON_PROPERTIES);
        getDecLayer().getStyleClass().add(RECTANGE_BUTTON_PROPERTIES);
        // Background color properties
        BackgroundColorBox.getStyleClass().add(SELECTOR_PROPERTIES);
        //Fill color Box properties
        FillPickerBox.getStyleClass().add(SELECTOR_PROPERTIES);
        // Outline color Box properties
        OutlinePickerBox.getStyleClass().add(SELECTOR_PROPERTIES);
        // thickness slider box properties
        ThicknessSliderBox.getStyleClass().add(LEFT_PANE_INNER_BOXES);
        // Snapshot button
        CameraArea.getStyleClass().add(LEFT_PANE_INNER_BOXES);
        getSnapShot().getStyleClass().add(SNAP_SHOT_PROPERTIES);
        
        
        
        
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
           resetButtonIndicator();
           deselectShape();
           if(!canvasBackgroundPane.getChildren().isEmpty()){
               selector.setDisable(false);
           }
           else{
               selector.setDisable(true);
           }



    }
    private void imageAttacher(ToggleButton facelessButton, String imageName){
        String appIcon = FILE_PROTOCOL + PATH_IMAGES + imageName +".png";
        facelessButton.setId(imageName);
        Image buttonImage = new Image(appIcon);
        facelessButton.setGraphic(new ImageView(buttonImage));
        
    }
    public void setCanvasBackground(Pane canvasBackground){
        this.canvasBackgroundPane = canvasBackground;
    }
    public Pane getCanvasBackground(){
        return canvasBackgroundPane;
    }
    public void setOutlineFill(ColorPicker outlineFill){
        this.outlineFill = outlineFill;
    }
    public ColorPicker getOutlineFill(){
        return outlineFill;
    }
    public void setFillColor(ColorPicker fillColor){
        this.fillColor = fillColor;
    }
    public ColorPicker getFillColor(){
        return fillColor;
    }
    public void setThicknessSlider(Slider thicknessSlider){
        this.thicknessSlider = thicknessSlider;
    }
    public Slider getThicknessSlider(){
        return thicknessSlider;
    }
    public void resetButtonIndicator(){
        
        getSelector().setSelected(false);
        getDeleter().setSelected(false);
        getSquare().setSelected(false);
        getEllipse().setSelected(false);
        getIncLayer().setSelected(false);
        getDecLayer().setSelected(false);
        getSnapShot().setSelected(false);
        
    }
    public Shape makeShape(Shape generalShape, double initX, double initY, Paint outlineColor, Paint fillColorPaint, double thickness){
        if(generalShape instanceof Rectangle){
            ((Rectangle)generalShape).setX(initX);
            ((Rectangle)generalShape).setY(initY);
            ((Rectangle)generalShape).setHeight(0);
            ((Rectangle)generalShape).setWidth(0);
        } else {
            ((Ellipse)generalShape).setCenterX(initX);
            ((Ellipse)generalShape).setCenterY(initY);
            ((Ellipse)generalShape).setRadiusX(0);
            ((Ellipse)generalShape).setRadiusY(0);
        }
        
        generalShape.setStrokeWidth(thickness);
        generalShape.setFill(fillColorPaint);
        generalShape.setStroke(outlineColor);
        
        generalShape.setOnMousePressed(e -> {

            if (selector.isSelected()) {

                if (getSelectedShape() == null || !selectedShape.equals(e.getPickResult().getIntersectedNode())) {
                    deselectShape();
                    setSelectedShape((Shape) e.getPickResult().getIntersectedNode());
                    selectedShape(getSelectedShape());

                }

            }

        });
        
        generalShape.setOnMouseEntered(e -> {
            if (selector.isSelected()) {
                app.getGUI().getPrimaryScene().setCursor(Cursor.OPEN_HAND);
            }
        });
        
        generalShape.setOnMouseDragged(e -> {

            if (selector.isSelected()) {
                updateGUIButtonsSelectedShapes(getSelectedShape());
                
                app.getGUI().getPrimaryScene().setCursor(Cursor.CLOSED_HAND);
                
                double moveAxisX = e.getX();
                double moveAxisY = e.getY();
                double rangeX;
                double rangeY;
               
                if (getSelectedShape() instanceof Ellipse) {
                    rangeX = moveAxisX - getSelectedShape().getStrokeWidth() - ((Ellipse)getSelectedShape()).getRadiusX();
                    rangeY = moveAxisY - getSelectedShape().getStrokeWidth() - ((Ellipse)getSelectedShape()).getRadiusY();
                } else {
                    rangeX = moveAxisX  - getSelectedShape().getStrokeWidth();
                    rangeY = moveAxisY  - getSelectedShape().getStrokeWidth();
                }
                
                if (!(rangeX < 0 || rangeY < 0)) {
                    if (getSelectedShape() instanceof Ellipse) {
                        ((Ellipse) getSelectedShape()).setCenterX(moveAxisX);
                        ((Ellipse) getSelectedShape()).setCenterY(moveAxisY);
                    } else {
                        ((Rectangle) getSelectedShape()).setX(moveAxisX);
                        ((Rectangle) getSelectedShape()).setY(moveAxisY);
                    }
                }
            }
        });
        
        generalShape.setOnMouseReleased(e -> {
            if (selector.isSelected()) {
                double moveAxisX = e.getX();
                double moveAxisY = e.getY();
                double rangeX;
                double rangeY;
                
                if (getSelectedShape() instanceof Ellipse) {
                    rangeX = moveAxisX - getSelectedShape().getStrokeWidth() - ((Ellipse)getSelectedShape()).getRadiusX();
                    rangeY = moveAxisY - getSelectedShape().getStrokeWidth() - ((Ellipse)getSelectedShape()).getRadiusY();
                } else {
                    rangeX = moveAxisX - getSelectedShape().getStrokeWidth();
                    rangeY = moveAxisY - getSelectedShape().getStrokeWidth();
                }
                
                if (!(rangeX < 0 || rangeY < 0)) {
                    if (getSelectedShape() instanceof Rectangle) {
                        
                        ((Rectangle) getSelectedShape()).setX(moveAxisX - (moveAxisX - ((Rectangle)getSelectedShape()).getX()));
                        ((Rectangle) getSelectedShape()).setY(moveAxisY - (moveAxisY - ((Rectangle)getSelectedShape()).getY()));
                    } else {
                        ((Ellipse) getSelectedShape()).setCenterX(moveAxisX - (moveAxisX - ((Ellipse)getSelectedShape()).getCenterX()));
                        ((Ellipse) getSelectedShape()).setCenterY(moveAxisY - (moveAxisY - ((Ellipse)getSelectedShape()).getCenterY()));
                    }
                    app.getGUI().getPrimaryScene().setCursor(Cursor.OPEN_HAND);
                } else {
                    AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
                    dialog.show("Alert", "Cannot move shape outside of Canvas, it will be put back on previous valid location");
                }
                app.getGUI().updateToolbarControls(false);
            }
        });
        
        generalShape.setOnMouseExited(e -> {
            if (selector.isSelected()) {
                app.getGUI().getPrimaryScene().setCursor(Cursor.DEFAULT);
            }
        });
        app.getGUI().updateToolbarControls(false);
        return generalShape;
    }
    
    public void highlightShape(Shape theShape){
        theShape.setStroke(HIGHLIGHT_COLOR);
        theShape.setStrokeWidth(HIGHLIGHT_SIZE);
    }

    /**
     * @return the selector
     */
    public ToggleButton getSelector() {
        return selector;
    }

    /**
     * @param selector the selector to set
     */
    public void setSelector(ToggleButton selector) {
        this.selector = selector;
    }

    /**
     * @return the deleter
     */
    public ToggleButton getDeleter() {
        return deleter;
    }

    /**
     * @param deleter the deleter to set
     */
    public void setDeleter(ToggleButton deleter) {
        this.deleter = deleter;
    }

    /**
     * @return the square
     */
    public ToggleButton getSquare() {
        return square;
    }

    /**
     * @param square the square to set
     */
    public void setSquare(ToggleButton square) {
        this.square = square;
    }

    /**
     * @return the ellipse
     */
    public ToggleButton getEllipse() {
        return ellipse;
    }

    /**
     * @param ellipse the ellipse to set
     */
    public void setEllipse(ToggleButton ellipse) {
        this.ellipse = ellipse;
    }

    /**
     * @return the incLayer
     */
    public ToggleButton getIncLayer() {
        return incLayer;
    }

    /**
     * @param incLayer the incLayer to set
     */
    public void setIncLayer(ToggleButton incLayer) {
        this.incLayer = incLayer;
    }

    /**
     * @return the decLayer
     */
    public ToggleButton getDecLayer() {
        return decLayer;
    }

    /**
     * @param decLayer the decLayer to set
     */
    public void setDecLayer(ToggleButton decLayer) {
        this.decLayer = decLayer;
    }

    /**
     * @return the snapShot
     */
    public ToggleButton getSnapShot() {
        return snapShot;
    }

    /**
     * @param snapShot the snapShot to set
     */
    public void setSnapShot(ToggleButton snapShot) {
        this.snapShot = snapShot;
    }
    public void deselectShape(){
        if(getSelectedShape() != null){

            getSelectedShape().setStroke(getTempBeforeHighlight());
            getSelectedShape().setStrokeWidth(getTempBeforeHighlightThickness());
            noShapeSelected();
        }
        setShapePressedAlready(false);
        deleter.setDisable(true);
        square.setDisable(false);
        ellipse.setDisable(false);
        
    }
    public void noShapeSelected(){
        setSelectedShape(null);
        setShapePressedAlready(false);
        deleter.setDisable(true);
        square.setDisable(false);
        ellipse.setDisable(false);
        incLayer.setDisable(true);
        decLayer.setDisable(true);
    }
    public void updateGUIButtonsSelectedShapes(Shape shape){
        incLayer.setSelected(false);
        decLayer.setSelected(false);
        Node ourSelectedShape = (Node) shape;
        int index = -1;
        for(Node shapes: canvasBackgroundPane.getChildren()){
            
            if(shapes.equals(ourSelectedShape)){
                index++;
                break;
            }
            index++;
        
        }
        int childrenSize = canvasBackgroundPane.getChildren().size() - 1;
        if( childrenSize == 0){
            decLayer.setDisable(true);
            incLayer.setDisable(true);
        }
        else{
            if(index - 1 < 0 ){
                incLayer.setDisable(true);
            }
            else{
                incLayer.setDisable(false);
            }
            if(index + 1 > childrenSize){
                decLayer.setDisable(true);
            }
            else{
                decLayer.setDisable(false);
            }
        }
      
        deleter.setDisable(false);
        square.setDisable(true);
        ellipse.setDisable(true);
        selector.setDisable(false);  
       
        
    }

    public void selectedShape(Shape shape) {
        setSelectedShape(shape);
        updateGUIButtonsSelectedShapes(getSelectedShape());
        setShapePressedAlready(true);
        outlineFill.setValue((Color) getSelectedShape().getStroke());
        fillColor.setValue((Color) getSelectedShape().getFill());
        thicknessSlider.setValue(getSelectedShape().getStrokeWidth());
        setTempBeforeHighlight(getSelectedShape().getStroke());
        setTempBeforeHighlightThickness(getSelectedShape().getStrokeWidth());

        highlightShape(getSelectedShape());
    }
    public void defaulinitLeftPanel(){
        selector.setDisable(true);
        deleter.setDisable(true);
        square.setDisable(false);
        ellipse.setDisable(false);
        backgroundFill.setValue(Color.WHITE);
        Paint backgroundColor =  backgroundFill.getValue();
        PageEditController.backgroundColorHandler(backgroundColor);
        outlineFill.setValue(Color.WHITE);
        fillColor.setValue(Color.WHITE);
        thicknessSlider.setValue(0);
        
        
    }

    public Slider getSlider(){
        return thicknessSlider;
    }
    public void setSlider(Slider thicknessSlider){
        this.thicknessSlider = thicknessSlider;
    }
    public ColorPicker getBackgroundFill(){
        return backgroundFill;
    }
    public void setBackgroundFill(ColorPicker back){
        backgroundFill = back;
    }

    /**
     * @return the tempBeforeHighlight
     */
    public Paint getTempBeforeHighlight() {
        return tempBeforeHighlight;
    }

    /**
     * @param tempBeforeHighlight the tempBeforeHighlight to set
     */
    public void setTempBeforeHighlight(Paint tempBeforeHighlight) {
        this.tempBeforeHighlight = tempBeforeHighlight;
    }

    /**
     * @return the tempBeforeHighlightThickness
     */
    public double getTempBeforeHighlightThickness() {
        return tempBeforeHighlightThickness;
    }

    /**
     * @param tempBeforeHighlightThickness the tempBeforeHighlightThickness to set
     */
    public void setTempBeforeHighlightThickness(double tempBeforeHighlightThickness) {
        this.tempBeforeHighlightThickness = tempBeforeHighlightThickness;
    }

    /**
     * @return the shapePressedAlready
     */
    public boolean isShapePressedAlready() {
        return shapePressedAlready;
    }

    /**
     * @param shapePressedAlready the shapePressedAlready to set
     */
    public void setShapePressedAlready(boolean shapePressedAlready) {
        this.shapePressedAlready = shapePressedAlready;
    }

    /**
     * @return the selectedShape
     */
    public Shape getSelectedShape() {
        return selectedShape;
    }

    /**
     * @param selectedShape the selectedShape to set
     */
    public void setSelectedShape(Shape selectedShape) {
        this.selectedShape = selectedShape;
    }

}
