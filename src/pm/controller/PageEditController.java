/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.paint.Paint; 
import javafx.scene.layout.Background;
import javafx.scene.layout.CornerRadii;
import pm.PoseMaker;
import pm.gui.Workspace;

import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author Jerrel
 */
public class PageEditController {
    PoseMaker app;
    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T THEMSELVES TRIGGER EVENTS
    private boolean enabled;  
    /**
     * Constructor for initializing this object, it will keep the app for later.
     *
     * @param initApp The JavaFX application this controller is associated with.
     */
    public PageEditController(PoseMaker initApp) {
	// KEEP IT FOR LATER
	app = initApp;
    }

    /**
     * This mutator method lets us enable or disable this controller.
     *
     * @param enableSetting If false, this controller will not respond to
     * workspace editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
	enabled = enableSetting;
    }
    
    public void backgroundColorHandler(Paint backgroundColor){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.getCanvasBackground().setBackground(new Background(new BackgroundFill(backgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
        
    }
     public void SnapShot(WritableImage image){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save Picture");
        fileChooser.setInitialFileName("Pose.png");
        File selectedFile = fileChooser.showSaveDialog(app.getGUI().getWindow());
         try{
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", selectedFile);
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
         }
         catch(IOException e){
            AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
            dialog.show("Alert", "Critical error while saving picture");
         }
     }
     
        


}
